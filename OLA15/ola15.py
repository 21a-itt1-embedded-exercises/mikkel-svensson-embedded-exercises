import machine
from time import sleep

led1 = machine.Pin(14, machine.Pin.OUT)
led2 = machine.Pin(15, machine.Pin.OUT)
btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
led_onboard = machine.Pin(25, machine.Pin.OUT)

def blink(led,count):
        for _ in range(count):
            led.value(1)
            sleep(0.1)
            led.value(0)
            sleep(0.1)

while True:
    if btn1.value()==0:
        blink(led1,1)
        blink(led_onboard,1)
    elif btn2.value()==0:
        blink(led2,1)
        blink(led_onboard,2)
