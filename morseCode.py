import machine
import time
from machine import Pin, PWM

string ="pidgeon"
array = list(string)
print(array)

buzzer = PWM(Pin(15))
buzzer.freq(1000)
led_onboard = machine.Pin(25, machine.Pin.OUT)
t = 0.05 #timing
def dot():
    led_onboard.toggle()
    buzzer.duty_u16(2000)
    time.sleep(t)
    led_onboard.toggle()
    buzzer.duty_u16(0)
    time.sleep(t)
def dash():
    led_onboard.toggle()
    buzzer.duty_u16(2000)
    time.sleep(3*t)
    led_onboard.toggle()
    buzzer.duty_u16(0)
    time.sleep(t)
def ss():#symbol space
    time.sleep(t)
def ls():#letterspace
    time.sleep(3*t)
def morseCode(arg):
    morseCodeString = arg
    mcarray = list(morseCodeString)
    for elem in mcarray:
        if elem == '.':
            dot()
        elif elem == '-':
            dash()
#alphabet
def write(arg1):
    if arg1.upper() == 'A':
        morseCode('.-')
    elif arg1.upper() == 'B':
        morseCode('-...')
    elif arg1.upper() == 'C':
        morseCode('-.-.')
    elif arg1.upper() == 'D':
        morseCode('-..')
    elif arg1.upper() == 'E':
        morseCode('.')
    elif arg1.upper() == 'F':
        morseCode('..-.')
    elif arg1.upper() == 'G':
        morseCode('--.')
    elif arg1.upper() == 'H':
        morseCode('....')
    elif arg1.upper() == 'I':
        morseCode('..')
    elif arg1.upper() == 'J':
        morseCode('.---')
    elif arg1.upper() == 'K':
        morseCode('-.-')
    elif arg1.upper() == 'L':
        morseCode('.-..')
    elif arg1.upper() == 'M':
        morseCode('--')
    elif arg1.upper() == 'N':
        morseCode('-.')
    elif arg1.upper() == 'O':
        morseCode('---')
    elif arg1.upper() == 'P':
        morseCode('.--.')
    elif arg1.upper() == 'Q':
        morseCode('.-.')
    elif arg1.upper() == 'R':
        morseCode('.-.')
    elif arg1.upper() == 'S':
        morseCode('...')
    elif arg1.upper() == 'T':
        morseCode('-')
    elif arg1.upper() == 'U':
        morseCode('..-')
    elif arg1.upper() == 'V':
        morseCode('...-')
    elif arg1.upper() == 'W':
        morseCode('-..-')
    elif arg1.upper() == 'X':
        morseCode('-..-')
    elif arg1.upper() == 'Y':
        morseCode('-.--')
    elif arg1.upper() == 'Z':
        morseCode('--..')
    elif arg1 == ' ':
        ls()
for element in array:
    write(element)
    ss()