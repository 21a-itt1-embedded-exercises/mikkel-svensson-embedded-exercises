#for Raspberry Pi Pico
class Potensiometer:
    
    def __init__(self,pin):
        from machine import ADC
        self.sensor = ADC(pin)

    def read_raw(self):
        return int(self.sensor.read_u16())
    
    def remap(self, value, maxInput, minInput, maxOutput, minOutput):
        value = maxInput if value > maxInput else value
        value = minInput if value < minInput else value
        inputSpan = maxInput - minInput
        outputSpan = maxOutput - minOutput
        scaledThrust = float(value - minInput) / float(inputSpan)
        return minOutput + (scaledThrust * outputSpan)
    
    def read_percentace(self):
        return self.remap(int(self.sensor.read_u16()), 65535, 300, 100,0)
    
    
pot1=Potensiometer(26)

print(pot1.read_raw())
