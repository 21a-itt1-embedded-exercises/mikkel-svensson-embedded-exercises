from machine import Pin, SoftI2C # from the machine library
import BME280 as bme280 # the bme280 library
from time import sleep
i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

bme = bme280.BME280(i2c=i2c, address=0x77)


while True:
    sleep(2)
    t = bme.read_compensated_data()[0]/100 #read_compensated_data() return centigrades
    h = bme.read_compensated_data()[2]/1024 
    if(t > 26):
        print("Turn down the heat")
    elif(t < 20):
        print("Did you open a window?")
    else:
        print("The temperature is perfect!")
    #CHALLENGE 
    if(h > 50):
        print("Humidity too high!")
    elif(h < 30):
        print("Humidity too low!")
    else:
        print("The Humidity perfect!")
    print(20*"-")
    #https://www.hvac.com/faq/recommended-humidity-level-home/