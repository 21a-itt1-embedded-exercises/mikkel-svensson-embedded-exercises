# this script assumes the connection of the I2C bus
# on devices is pin0 = SDA, pin1 = scl

from machine import Pin, SoftI2C # from the machine library

i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

devices = i2c.scan()

if devices:
    for d in devices:
        print(hex(d))
