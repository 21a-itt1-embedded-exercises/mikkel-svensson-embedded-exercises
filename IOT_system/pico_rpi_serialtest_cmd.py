import serial
from time import sleep

def read_uart(connection):
    msg = connection.readline() or bytes('error')
    msg = msg.decode('utf-8').rstrip()
    return msg

def write_uart(connection, message):
    message = message + '\n'
    connection.write(bytes(message, 'utf-8'))

try:
    with serial.Serial(port='/dev/serial0', baudrate=19200, bytesize=8, parity='N', stopbits=1) as ser:
        while True:
            write_uart(ser, 'temperature')
            sleep(1)
            print(read_uart(ser))
            write_uart(ser, 'humidity')
            sleep(1)
            print(read_uart(ser))
            write_uart(ser, 'pressure')
            sleep(1)
            print(read_uart(ser))

except KeyboardInterrupt:
    print('program closed.....')
