import serial
from time import sleep
from gpiozero import Button

from PCF8574 import PCF8574_GPIO
from Adafruit_LCD1602 import Adafruit_CharLCD

from publish import publish

import re
def destroy():
	lcd.clear()
	mcp.output(3,0)

def read_uart(connection):
    msg = connection.readline() or bytes('error')
    msg = msg.decode('utf-8').rstrip()
    return msg

def write_uart(connection, message):
    message = message + '\n'
    connection.write(bytes(message, 'utf-8'))

PCF8574_address = 0x27
PCF8574A_address = 0x3F
try:
    mcp = PCF8574_GPIO(PCF8574_address)
except:
    try:
        mcp = PCF8574_GPIO(PCF8574A_address)
    except:
        print ('I2C Address Error !')
        exit(1)

lcd = Adafruit_CharLCD(pin_rs=0, pin_e=2, pins_db=[4,5,6,7], GPIO=mcp)

mcp.output(3,1)     # turn on LCD backlight
lcd.begin(16,2)     # set number of LCD lines and columns

def print_uart_to_1602(ser,message,coloumn,row):
	write_uart(ser, message)
	lcd.setCursor(coloumn,row)
	data=read_uart(ser)
	lcd.message(data)
	return re.findall("[0-9]+\.[0-9]+",data)[0]

#buttons
button0 = Button(6)
button1 = Button(13)
button2 = Button(19)
try:
	with serial.Serial(port='/dev/serial0', baudrate=19200, bytesize=8, parity='N', stopbits=1) as ser:
		while True:
			if button0.when_pressed:
				publish([1,[print_uart_to_1602(ser,'temperature',0,0)]])
			if button1.when_pressed:
				publish([2,[print_uart_to_1602(ser,'humidity',10,0)]])
			if button2.when_pressed:
				publish([3,[print_uart_to_1602(ser,'pressure',3,1)]])
except KeyboardInterrupt:
	print('program closed.....')
	destroy()
